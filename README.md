# Processo Seletivo

Processo Seletivo para a vaga de Web Designer

Criei dois arquivos index.html e index2.html, apresentando o resultado da consulta em duas pespectivas, a primeira utilizando tabela e a segunda utilizando cards.

Nos arquivos adicionei as folhas de estilo do Bootstrap e Font Awesome para formatar a página, também coloquei os formulários dentro de um layout de sistema fictício, para fazer uso da logomarca no NavBar.

Qualquer comentário pode me enviar um e-mail no joaovictorweb@gmail.com

Espero fazer parte deste time!